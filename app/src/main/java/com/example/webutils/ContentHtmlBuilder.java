package com.example.webutils;

import android.app.Activity;

import com.example.ddsport.R;
import com.example.models.posts.Post;

public class ContentHtmlBuilder {
    public static String title(String title){
        String html = "";
        html+= "<h1 style=\'font-size: 80px;\'>"+title+"</h1>";
        return html;
    }

    public static String get_img(String url){
        String html = "";
        html+= "<img src=\""+url+"\" style=\"margin: 0 auto;width: 100%; height: auto;\" >";
        return html;
    }

    public static String get_video(String url){
        String html="";
        html+="<video style=\"margin: 0 auto;width: 100%; height: auto;\" controls preload=\"auto\">";
        html+="<source src=\""+url+"\" type='video/mp4'>";
        html+="</video>";
        return html;
    }

    public static String get_details(Activity context, String at, String posted_by, String[]tags){
        String html="";
        html+="<ul style=\"font-size: 50px;\">";
        String at_label= context.getResources().getText(R.string.posted_at).toString();
        html+="<li>"+at_label+": <span style=\"font-weight: bold;\">"+at+"</span></li>";
        String posted_by_label=  context.getResources().getText(R.string.posted_by).toString();
        html+="<li>"+posted_by_label+": <span style=\"font-weight: bold;\">"+posted_by+"</span></li>";
        html+="<li>";
        for(String tag: tags){
            html+="#"+tag+" ";
        }
        html+="</li>";
        html+="</ul>";
        return html;
    }
    public static String get_web_view(Activity context, String title,
                                      String type, String url, String at,
                                      String posted_by,String[]tags, int width){

        String html="<html>";
        html+="<head><title>POST</title></head>";
        html+="<body>";
        html+="<div style=\"text-align: center; width: "+width+"px\">";
        html+= title(title);
        html+="<hr>";
        if (type.equals("image") || type.equals("jpg") || type.equals("png")|| type.equals("gif")){
            html+=get_img(url);

        }else if (type.equals("video")){
            html+=get_video(url);
        }
        html+="<hr>";
        html+=get_details(context, at, posted_by, tags);
        html+="<hr>";
        html+="</div>";
        html+="</body>";
        html+="</html>";
        return html;
    }
}
