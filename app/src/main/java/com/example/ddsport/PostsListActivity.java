package com.example.ddsport;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentTransaction;

import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import com.example.models.posts.Post;
import com.example.models.users.User;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.Timestamp;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Map;

public class PostsListActivity extends AppCompatActivity implements PopupMenu.OnMenuItemClickListener {
    Post[] posts;
    ImageView menuIcon;
    SharedPreferences sharedPreferences;
    private static final String TAG = "PostsListActivity";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        sharedPreferences = getSharedPreferences(SplashScreenActivity.MyPREFERENCES, Context.MODE_PRIVATE);
        setContentView(R.layout.posts_list);
        // Setting header
        ListView listView = findViewById(R.id.list);
        TextView textView = new TextView(this);
        textView.setTypeface(Typeface.DEFAULT_BOLD);
        textView.setText(R.string.home_message);
        listView.addHeaderView(textView);
        init();



    }
    @Override
    protected void onStart() {
        super.onStart();
        //
        PostsListActivity ctx = this;
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        //bouton menu
        menuIcon = findViewById(R.id.app_menu);
        Log.d(TAG, menuIcon.toString());
        menuIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PopupMenu popup = new PopupMenu(PostsListActivity.this, v);
                popup.setOnMenuItemClickListener(PostsListActivity.this);
                popup.inflate(R.menu.menu_options);
                popup.show();
            }
        });
        //eventRecherche
        EditText search_input =  findViewById(R.id.etSearchPost);
        search_input.addTextChangedListener(new TextWatcher() {

            public void afterTextChanged(Editable s) {
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String searchKey = s.toString() + "\uf8ff";
                db.collection("contents")
                        .orderBy("title")
                        .startAt(s.toString())
                        .endAt(searchKey)
                        .limit(3)
                        .get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                            @Override
                            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                                ArrayList<Post> res = new ArrayList<Post>();
                                if (task.isSuccessful()) {
                                    for (QueryDocumentSnapshot document : task.getResult()) {
                                        Post post = createPost(document);
                                        res.add(post);
                                        Log.d(TAG, document.getId() + " => " + document.getData() + ", posts:" + res.size());
                                    }
                                    posts = res.toArray(new Post[res.size()]);
                                    System.out.println(posts.length);
                                    // For populating list data
                                    ListView listView = findViewById(R.id.list);

                                    CustomPostList customPostList = new CustomPostList(ctx, posts);
                                    listView.setAdapter(customPostList);

                                    listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                        @Override
                                        public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                                            Toast.makeText(getApplicationContext(), "You Selected " + posts[position - 1].getTitle() + " - " + position, Toast.LENGTH_SHORT).show();
                                            Post p = posts[position-1];
                                            goToDetails(p);
                                        }
                                    });

                                } else {
                                    Log.w(TAG, "Error getting documents.", task.getException());
                                }
                            }
                        });
            }
        });

        PostsListActivity activity_instance = this;
        db.collection("contents").orderBy("at").limit(3).get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        ArrayList<Post> res = new ArrayList<Post>();
                        if (task.isSuccessful()) {
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                Post post = createPost(document);
                                res.add(post);
                                Log.d(TAG, document.getId() + " => " + document.getData() + ", posts:" + res.size());
                            }
                            posts = res.toArray(new Post[res.size()]);
                            System.out.println(posts.length);
                            // For populating list data
                            ListView listView =  findViewById(R.id.list);

                            CustomPostList customPostList = new CustomPostList(activity_instance, posts);
                            listView.setAdapter(customPostList);

                            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                @Override
                                public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                                    Toast.makeText(getApplicationContext(), "You Selected " + posts[position - 1].getTitle() + " - " + position + " - "+posts[position-1].getPoster().getPseudo() , Toast.LENGTH_SHORT).show();
                                    Post p = posts[position-1];
                                    goToDetails(p);
                                }
                            });

                        } else {
                            Log.w(TAG, "Error getting documents.", task.getException());
                        }
                    }
                });
    }
    private Post createPost(QueryDocumentSnapshot document){
        Post post = new Post();
        post.setId(document.getId());
        post.setTitle(document.getData().get("title").toString());
        Timestamp at = document.getTimestamp("at");
        post.setAt(at);
        ArrayList<String> tags = (ArrayList<String> )document.getData().get("tags");
        post.setTags(tags.toArray(new String[tags.size()]));
        Map<String,String> content = (Map<String,String>)document.getData().get("content");
        //post.setPost_img(content.get("type"), content.get("link"));
        post.setContent_type(content.get("type"));
        post.setContent_url(content.get("link"));
        post.setPost_thumbnail(document.get("thumbnail").toString());
        DocumentReference user_ref = (DocumentReference) document.getData().get("user");
        user_ref.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if (task.isSuccessful()){
                    DocumentSnapshot doc = task.getResult();
                    User u = new User();
                    u.setId(doc.getId());
                    u.setPseudo(doc.get("username",String.class));
                    post.setPoster(u);
                }
            }
        });
        return post;
    }
    private void goToDetails(Post p){
        String post_id= p.getId();
        String post_title= p.getTitle();
        String poster_pseudo= p.getPoster().getPseudo();
        String[] post_tags = p.getTags();
        String post_type = p.getContent_type();
        String post_src= p.getContent_url();
        DateFormat dateFormat = android.text.format.DateFormat.getDateFormat(getApplicationContext());
        String posted_at = dateFormat.format(p.getAt().toDate());

        Intent intent = new Intent(PostsListActivity.this, PostDetailActivity.class);
        Bundle bundle = new Bundle();
        bundle.putString("post_id",post_id);
        bundle.putString("post_title",post_title);
        bundle.putString("post_src",post_src);
        bundle.putString("post_type",post_type);
        bundle.putString("user_pseudo",poster_pseudo);
        bundle.putString("posted_at",posted_at);
        bundle.putStringArray("post_tags",post_tags);
        intent.putExtras(bundle);
        startActivity(intent);
        finish();
    }
    private void init() {
        ToolbarFragment fragment = new ToolbarFragment();
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        // replace whatever is in the fragment_container view with this fragment,
        // and add the transaction to the back stack so the user can navigate back.
        transaction.replace(R.id.fragment_container, fragment);
//        transaction.addToBackStack(null); // Because we have only one fragment that why we haven't added this line. In case you have multiple fragment include this line.
        transaction.commit();
    }
    @Override
    public boolean onMenuItemClick(MenuItem item) {
        Toast.makeText(this, "Selected Item: " +item.getTitle(), Toast.LENGTH_SHORT).show();
        if (item.getItemId() == R.id.disconnect) {// do your code
            Log.d(TAG, "...");
            sharedPreferences.edit().putString("user_id", "").apply();
            startActivity(new Intent(PostsListActivity.this, SplashScreenActivity.class));
            return true;
        }
        return false;
    }
}
