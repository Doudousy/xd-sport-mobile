package com.example.ddsport;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

public class LoginActivity extends AppCompatActivity {
    Button login_btn;
    EditText usr_name, usr_pwd;
    String user_name_demo = "demo";
    String user_password_demo = "1234";
    private static final String TAG = "LoginActivity";
    SharedPreferences sharedpreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_page);
        login_btn = (Button) findViewById(R.id.login_button);
        usr_name = (EditText) findViewById(R.id.username);
        usr_pwd = (EditText) findViewById(R.id.userpassword);

        login_btn.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                if(
                        usr_name.getText().toString().equals(user_name_demo)
                        && usr_pwd.getText().toString().equals(user_password_demo)
                ){
                    Toast.makeText(getApplicationContext(), "Using demo user ... Redirecting ...", Toast.LENGTH_SHORT).show();
                    sharedpreferences = getSharedPreferences(SplashScreenActivity.MyPREFERENCES, Context.MODE_PRIVATE);
                    sharedpreferences.edit().putString("user_id","demo").apply();
                    startActivity(new Intent(LoginActivity.this, PostsListActivity.class));
                }else{
                    FirebaseFirestore db = FirebaseFirestore.getInstance();
                    db.collection("users").whereEqualTo("username",usr_name.getText().toString())
                            .whereEqualTo("password",usr_pwd.getText().toString())
                            .get()
                            .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                        @Override
                        public void onComplete(@NonNull Task<QuerySnapshot> task) {
                            if (task.isSuccessful()) {
                                String user_id = "";
                                for (QueryDocumentSnapshot document : task.getResult()) {
                                    user_id = document.getId();
                                    Log.d(TAG, document.getId() + " => " + document.getData());
                                }
                                sharedpreferences = getSharedPreferences(SplashScreenActivity.MyPREFERENCES, Context.MODE_PRIVATE);
                                SharedPreferences.Editor editor = sharedpreferences.edit();
                                editor.putString("user_id", user_id);
                                editor.apply();
                                startActivity(new Intent(LoginActivity.this, PostsListActivity.class));

                            } else {
                                Log.w(TAG, "Error getting documents.", task.getException());
                            }
                        }
                    });
                }
            }
        });
    }


}
