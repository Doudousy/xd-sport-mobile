package com.example.ddsport;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Base64;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;
import android.webkit.WebSettings;
import android.webkit.WebView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.example.webutils.ContentHtmlBuilder;
import com.example.models.posts.Post;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.Arrays;

public class PostDetailActivity extends AppCompatActivity {
    String post_id;
    String post_title;
    String[] post_tags;
    String post_src;
    String post_type;
    String user_pseudo;
    String posted_at;

    //Charger user et media

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle b = getIntent().getExtras();
        setContentView(R.layout.post_details);
        post_id = b.getString("post_id","");
        post_title = b.getString("post_title","");
        post_src = b.getString("post_src","");
        post_type = b.getString("post_type","");
        user_pseudo = b.getString("user_pseudo","");
        posted_at = b.getString("posted_at","");
        post_tags = b.getStringArray("post_tags");

        Toolbar mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(PostDetailActivity.this, PostsListActivity.class));
                finish();
            }
        });

    }

    @Override
    protected void onStart() {
        super.onStart();
        WebView wv;
        wv = (WebView) findViewById(R.id.post_details);
        Display display = ((WindowManager) getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();
        int width = display.getWidth();
        String unencodedHtml = ContentHtmlBuilder.get_web_view(this, post_title,
                post_type, post_src, posted_at, user_pseudo,post_tags, width);
        String encodedHtml = Base64.encodeToString(unencodedHtml.getBytes(),
                Base64.NO_PADDING);
        wv.getSettings().setLoadWithOverviewMode(true);
        wv.getSettings().setUseWideViewPort(true);
        wv.loadData(encodedHtml, "text/html", "base64");

    }
}
