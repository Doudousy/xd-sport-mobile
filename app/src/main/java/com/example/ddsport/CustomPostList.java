package com.example.ddsport;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.models.posts.Post;

public class CustomPostList extends ArrayAdapter {
    Post[]posts;
    private Activity context;
    public CustomPostList(Activity context, Post[]posts){
        super(context, R.layout.post_item, getPostsTitles(posts));
        this.context = context;
        this.posts = posts;
    }
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row=convertView;
        LayoutInflater inflater = context.getLayoutInflater();
        if(convertView==null)
            row = inflater.inflate(R.layout.post_item, null, true);
        TextView textPostTitle = (TextView) row.findViewById(R.id.post_title);
        ImageView postImage = (ImageView) row.findViewById(R.id.post_image);

        textPostTitle.setText(posts[position].getTitle());
        postImage.setImageBitmap(posts[position].getPost_thumbnail());
        return  row;
    }
    public static String[]getPostsTitles(Post[]posts){
        String[]titles = new String[posts.length];
        for(int i=0;i<titles.length;i++){
            titles[i] = posts[i].getTitle();
        }
        return  titles;
    }

}
