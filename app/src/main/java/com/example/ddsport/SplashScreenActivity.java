package com.example.ddsport;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

public class SplashScreenActivity extends AppCompatActivity {
    public static final String MyPREFERENCES = "MyPrefs" ;
    SharedPreferences sharedpreferences;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        if (sharedpreferences.getString("user_id","").equals("")) {
            startActivity(new Intent(SplashScreenActivity.this, LoginActivity.class));
            finish();
        }else{
            startActivity(new Intent(SplashScreenActivity.this, PostsListActivity.class));
            finish();
        }
    }
}