package com.example.models.posts;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;

import com.example.models.users.User;
import com.google.firebase.Timestamp;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.concurrent.ExecutionException;

public class Post {
    private String id;
    private String title = "OOF";
    private String content_url;
    private String content_type;
    private String[]tags;
    private Timestamp at;
    private Bitmap post_img;
    private Bitmap post_thumbnail;
    private User poster;
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
    public Post(){
        this.id = "";
    }

    public Post(String m_id, String m_title){
        setId(m_id);
        setTitle(m_title);
    }

    public Bitmap getPost_img() {
        return post_img;
    }

    public void setPost_img(Bitmap post_img) {
        this.post_img = post_img;
    }
    public void setPost_img(String filetype, String url){
        setContent_type(filetype);
        setContent_url(url);
        try {
            if(filetype.toLowerCase().equals("jpg") || filetype.toLowerCase().equals("png")) {
                Bitmap bitmap = new DownloadImgTask().execute(url).get();
                if(bitmap!=null){
                    this.post_img=bitmap;
                }
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }

    }
    public void setPost_thumbnail(String url){
        try {
                Bitmap bitmap = new DownloadImgTask().execute(url).get();
                if(bitmap!=null){
                    this.post_thumbnail=bitmap;
                }
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
    }

    public String getContent_type() {
        return content_type;
    }

    public void setContent_type(String content_type) {
        this.content_type = content_type;
    }

    public String getContent_url() {
        return content_url;
    }

    public void setContent_url(String content_url) {
        this.content_url = content_url;
    }

    public User getPoster() {
        return poster;
    }

    public void setPoster(User poster) {
        this.poster = poster;
    }

    public String[] getTags() {
        return tags;
    }

    public void setTags(String[] tags) {
        this.tags = tags;
    }

    public Bitmap getPost_thumbnail() {
        return post_thumbnail;
    }

    public void setPost_thumbnail(Bitmap post_thumbnail) {
        this.post_thumbnail = post_thumbnail;
    }

    public Timestamp getAt() {
        return at;
    }

    public void setAt(Timestamp at) {
        this.at = at;
    }
}
class DownloadImgTask extends AsyncTask<String, Void, Bitmap> {

    private Exception exception;

    protected Bitmap doInBackground(String... urls) {
        InputStream is = null;
        try {
            URL url = new URL(urls[0]);
            is = (InputStream) url.getContent();
            Bitmap bitmap = BitmapFactory.decodeStream(is);
            return bitmap;
        } catch (Exception e) {
            this.exception = e;

            return null;
        } finally {
            if (is != null) {
                try {
                    is.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    protected void onPostExecute(Bitmap bmp) {
        // TODO: check this.exception
        System.out.println(this.exception);
        // TODO: do something with the bmp
    }
}
